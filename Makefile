start:
	- docker-compose up

up-build:
	- docker-compose up --build

stop:
	- docker-compose down

fix:
	- docker-compose exec flow npm audit fix --force	

buildapp:
	- docker-compose exec flow npm run build