import { Link } from "react-router-dom"

const Footer = () => {
    return ( 
        <div className="rf-container rf-mt-10w">
            <div className="rf-footer__body rf-mt-5w">
                <div className="rf-footer__brand">
                    <a title="Ministère des Armées" className="rf-logo" href="https://www.defense.gouv.fr/">
                        <span className="rf-logo__title">
                        <span>Ministère <br />des Armées</span></span>
                    </a>

                    <a title="Ministère des Armées" className="rf-ml-5w" href="https://www.civils.defense.gouv.fr/" style={{boxShadow:"none"}}>
                        <img alt="" className="rf-footer__logo" src="https://osu.eu-west-2.outscale.com/cvd-production/organizations/partner_1_logos/957/365/2a-/original/Logo_CivilsDeLaDefense.svg"/>
                    </a>
                </div>
                <div className="rf-footer__content">
                    <p className="rf-footer__content-desc">Site temporaire du
                        Ministère des Armées dans le cadre du Hackathon GES / Eductive<br/><b>SRHC/SDGPC/DCC</b>.</p>
                    <ul className="rf-footer__content-list">
                        <li>
                            <a className="rf-footer__content-link" target="_blank"
                                href="https://www.service-public.fr">Service-public.fr</a>
                        </li>
                        <li>
                            <a className="rf-footer__content-link" target="_blank"
                                href="https://www.legifrance.gouv.fr">Legifrance.gouv.fr</a>
                        </li>
                        <li>
                            <a className="rf-footer__content-link" target="_blank"
                                href="https://www.data.gouv.fr">Data.gouv.fr</a>
                        </li>
                        <li>
                            <a className="rf-footer__content-link" target="_blank"
                                href="https://www.france.fr">France.fr</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="rf-footer__bottom">
                <ul className="rf-footer__bottom-list">
                    <li className="rf-footer__bottom-item">
                        <a className="rf-footer__bottom-link" href="#">Accessibilité : non</a>
                    </li>
                    <li className="rf-footer__bottom-item">
                        <a className="rf-footer__bottom-link" href="#">Mentions légales</a>
                    </li>
                    <li className="rf-footer__bottom-item">
                        <a className="rf-footer__bottom-link" href="#">Conditions générales d’utilisation</a>
                    </li>
                    <li className="rf-footer__bottom-item">
                        <a className="rf-footer__bottom-link" href="#">Politique de confidentialité</a>
                    </li>
                    <li className="rf-footer__bottom-item">
                        <a className="rf-footer__bottom-link" href="#">Suivi d'audience et vie privée</a>
                    </li>
                    <li className="rf-footer__bottom-item">
                        <Link to="contact"className="rf-footer__bottom-link" target="_self">Nous contacter</Link>
                    </li>
                </ul>
                <div className="rf-footer__bottom-copy">
                    © Ministère des Armées 2021
                </div>
            </div>
        </div>
    );
}
 
export default Footer;