import { Link } from "react-router-dom"

const NotFound = () => {
    return (
    <>
        <main>
            <section className="filter">
                <div className="spinner">
                    <h1>Page introuvable</h1>
                    <p className="rf-mb-3w">Désolé, mais la page demandée n’existe pas.</p>
                    <Link to="/" className="rf-btn rf-btn--secondary" target="_self">Retour sur la page d'accueil</Link>
                </div>
            </section>
        </main>
    </>);
}
 
export default NotFound;