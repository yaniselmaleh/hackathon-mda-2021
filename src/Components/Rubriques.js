import { HashLink } from 'react-router-hash-link';
// import FrequentlyAskedQuestions from './FrequentlyAskedQuestions';

const Rubriques = () => {
    return ( 
        <div className="rf-container">
            <div className="rf-grid-row rf-grid-row--gutters rf-mt-5w">
                <div className="rf-col">
                    <h2>Rubriques</h2>
                </div>
            </div>
            <div className="rf-grid-row rf-grid-row--gutters rf-mb-5w">
                <div className="rf-col-12 rf-col-md-4">
                    <HashLink to="/arborescence">
                        <div className="rf-card">
                            <div className="rf-card__body">
                                <h2 className="rf-card__title"><a href="#">Arborescence</a></h2>
                                <p className="rf-card__desc">Un arbre décisionnel des différents échanges possibles avec le chatbot est fourni en annexe.</p>
                            </div>
                        </div>
                    </HashLink>
                </div>
                <div className="rf-col-12 rf-col-md-4">
                    <HashLink to="/articles#root">
                    <div className="rf-card">
                        <div className="rf-card__body">
                            <h2 className="rf-card__title">
                                <a href="#">Contenu</a></h2>
                            <p className="rf-card__desc">Ensembles de vocabulaires et lexiques disponibles en annexe.</p>
                        </div>
                    </div>
                    </HashLink>
                </div>

                <div className="rf-col-12 rf-col-md-4">
                    <HashLink to="/rappel#root">
                    <div className="rf-card">
                        <div className="rf-card__body">
                            <h2 className="rf-card__title">
                                <a href="#">Rappel</a></h2>
                            <p className="rf-card__desc">Memo sur les challenges, fonctionnalités attendu et contraintes lié au Ministère.</p>
                        </div>
                    </div>
                    </HashLink>
                </div>

                <div className="rf-col-12 rf-col-md-4">
                    <HashLink to="/contact#root">
                    <div className="rf-card">
                        <div className="rf-card__body">
                            <h2 className="rf-card__title">
                                <a href="#">Contact</a></h2>
                            <p className="rf-card__desc">Foire au questions sur l'évènement, informations pour contacter léquipe projet</p>
                        </div>
                    </div>
                    </HashLink>
                </div>
            </div>
        
            <div className="rf-callout rf-fi-information-line">
                <h4 className="rf-callout__title">Civils de la Défense</h4>
                    <p className="rf-callout__text">
                    Chaque année, le ministère des Armées propose près de 5 000 postes à des civils. Pour faciliter l’accès aux informations et aux offres d’emploi, le Secrétariat général pour l’administration (SGA) lance un portail unique.
                    </p>
                <a href="https://www.civils.defense.gouv.fr/" className="rf-btn" target="_self" title="civils.defense.gouv.fr - [Ouvrir dans une nouvelle fenêtre]">En savoir plus</a>
            </div>
            {/* <FrequentlyAskedQuestions/> */}
        </div>
    );
}
export default Rubriques;