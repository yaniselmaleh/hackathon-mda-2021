import { HashLink } from 'react-router-hash-link';
import Header from './Header';

const TreeChoice = () => {
    return ( 
        <>
        <Header/>
        <div className="rf-container">
            <div className="rf-grid-row rf-grid-row--gutters rf-mt-5w">
                <div className="rf-col">
                    <h2>Arborescence Web &amp; Bot</h2>
                </div>
            </div>
            <div className="rf-grid-row rf-grid-row--gutters rf-mb-5w">
                <div className="rf-col-12 rf-col-md-6">
                    <HashLink to="/arborescence-bot#root">
                        <div className="rf-card">
                            <div className="rf-card__body">
                                <h2 className="rf-card__title"><a href="#">Arborescence Bot</a></h2>
                                <p className="rf-card__desc">Un arbre décisionnel des différents échanges possibles avec le chatbot</p>
                            </div>
                        </div>
                    </HashLink>
                </div>
                <div className="rf-col-12 rf-col-md-6">
                    <HashLink to="/arborescence-web#root">
                    <div className="rf-card">
                        <div className="rf-card__body">
                        <h2 className="rf-card__title"><a href="#">Arborescence Site Web</a></h2>
                            <p className="rf-card__desc">Ensemble de rubriques demandées (à minima) pour site vitrine.</p>
                        </div>
                    </div>
                    </HashLink>
                </div>
            </div>
        </div>
    </>
    );
}
 
export default TreeChoice;