import React, {useEffect, useState} from 'react';
import Parser from 'html-react-parser';
import Header from './Header';
import Footer from './Footer';

const Lexiques = ({lexiques}) => {
  const [search, setSearch] = useState('')
  const [filteredData, setFilteredData] = useState([]);

  const filterData = (search) => {
    setSearch(search);
    setFilteredData(
      lexiques.filter((lexiques) =>
        lexiques.description.toLowerCase().includes(search.toLowerCase())
      )
    )
  }

  useEffect(() => {
    if(search === ''){
      setFilteredData(lexiques);
    }
  }, [search])

  return (
    <>
      <Header/>
        <div className="rf-container rf-my-5w">
          <h1>Lexique, dispositifs de formation</h1>
          <p>Vocabulaire mis à disposition - Ministère des Armées</p>

          <div className="rf-grid-row rf-grid-row-gutters rf-mt-5w">
              <div className="rf-col-12 rf-col-xs-12">
                  <div className="rf-search-bar" id="search-input">
                      <label className="rf-label" for="search-input-input">Label de la barre de recherche</label>
                      <input className="rf-input" placeholder="Rechercher" type="search" onChange={(e) => filterData(e.target.value)} id="search-input-input" name="search-input-input"/>
                      <button className="rf-btn" title="Rechercher">Rechercher</button>
                  </div>
              </div>
          </div>
        </div>
        
          {
            filteredData && filteredData.map((lexique) => (
              <>
                <div className="rf-container rf-my-5w">
                  <div className="rf-grid-row rf-grid-row--gutters rf-mb-5w">
                    <div className="rf-col-12">
                        <div className="rf-card">
                          <div style={{padding:"1.5rem"}} key={lexique.id}>
                            <h2 className="rf-card__title"><a href="#">{lexique.titre}</a></h2>
                            <p className="rf-card__desc">{Parser(lexique.description)}</p>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              </>
            )
          ) 
          }
          {
            filteredData && filteredData.length === 0 && (
              <div className="rf-container rf-my-5w">
                <h2>Aucune données !</h2>
              </div>
            )
          }
      <Footer/> 
      </>
    );
  };

export default Lexiques;