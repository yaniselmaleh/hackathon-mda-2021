import Hackathon from "../img/GES_RS_Hackathon_GES.jpg"
import { HashLink } from 'react-router-hash-link';

const Promote = () => {
    return ( 
        <>
            <div className="rf-container">
                <div className="rf-grid-row rf-grid-row--center rf-grid-row--middle rf-mt-5w">
                    <div className="rf-col-lg-6 rf-col-xs-12 rf-mr-10w">
                        <h1>
                            Organisation de la formation au sein du Ministère des Armées
                        </h1>
                        <div className="rf-pb-5w">
                        La formation professionnelle est un droit dont dispose les agents publics afin d’évoluer professionnellement, de renforcer leurs compétences et d’envisager une reconversion professionnelle.
                        </div>
                        <div>
                            <HashLink className="rf-btn" title="Voir les missions" to="/rappel#root">
                                Voir les missions
                            </HashLink>
                        </div>
                    </div>
                    <div className="rf-col-4 rf-mt-5w">
                        <img src={Hackathon} alt="Candidatez aux missions" style={{width: "100%"}} />
                    </div>
                </div>
            </div>
            
            <div className="rf-container-fluid rf-bg--alt rf-centered rf-grid-row--center rf-pt-3w rf-pb-3w rf-mt-5w" id="promote">
                <div className="rf-grid-row rf-grid-row--center rf-grid-row--gutter">
                    <div className="rf-col-xs-12 rf-col-md-3 rf-p-3w">
                        <span className="rf-fi-checkbox-line rf-fi--lg rf-pb-2w text-green-soft"></span>
                        <div className="rf-text--sm">Montée en compétences</div>
                    </div>
                    <div className="rf-col-xs-12 rf-col-md-3 rf-p-3w">
                        <span className="rf-fi-calendar-line rf-fi--lg rf-pb-2w text-orange-soft"></span>
                    <div className="rf-text--sm">Une semaine de challenges</div>
                    </div>
                    <div className="rf-col-xs-12 rf-col-md-3 rf-p-3w">
                        <span className="rf-fi-information-line rf-fi--lg rf-pb-2w text-blue-soft"></span>
                    <div className="rf-text--sm">Ministère des Armées</div>
                    </div>
                </div>
            </div>
        </>
    );
}
 
export default Promote;