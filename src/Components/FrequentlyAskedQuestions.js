import Faq from 'react-faq-component';

const FrequentlyAskedQuestions = (props) => {  
    const data = {
        title: "",
        rows: [
          {
            title  : "Quel est le but du Hackathon ?",
            content: "L’équipe projet a pour volonté de créer un nouvel outil, innovant et intuitif, permettant aux agents du ministère de s’orienter vers le dispositif de formation le plus adapté à leur projet professionnel.Les dispositifs de formation sont nombreux, avec des finalités diverses, parfois complémentaires, et il n’est parfois pas évident pour les agents de trouver celui qui correspond à leurs objectifs.<br/><br/>Cet outil d’aide à la décision s’inscrit dans la stratégie globale du BFCPE, visant à soutenir les agents dans leurs démarches d’évolution professionnelle.Le challenge, pour vous, est donc de créer cet outil, en vous basant sur les documents annexes fournis.<br/><br/>Le caractère évolutif du domaine de la formation impose un outil administrable sans avoir à intervenir sur le code (et donc un back-office ultra-personnalisable, type wordpress), permettant d’effectuer sans le soutien de l’IT toutes les modifications possibles.Votre mission sera de créer cet outil, axé sur un site vitrine et un outil d’aide à la décision semi-dirigé (type chatbot) …<br/><br/>Le site vitrine devra valoriser plusieurs entrées, le catalogue de formation (administrable en back-office après injection des données issues du SIRH) des ressources documentaires, un annuaire, des rubriques d’informations et de vulgarisation, une page actualité…Le chatbot permettra d’orienter l’agent vers le dispositif de formation le plus adapté à ses besoins à travers les choix qu’il fera selon les propositions. "
          },
          {
            title  : "Où se trouvent les consignes complètes et détaillées du challenge ?",
            content: "Nunc maximus, magna at ultricies elementum, risus turpis vulputate quam."
          },
          {
            title  : "Comment se déroule la semaine du Hackathon ?",
            content: "Curabitur laoreet, mauris vel blandit fringilla, leo elit rhoncus nunc"
          },
          {
            title  : "Quelle est la composition du jury ?",
            content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed tempor sem. Aenean vel turpis feugiat, ultricies metus at, consequat velit. Curabitur est nibh, varius in tellus nec, mattis pulvinar metus. In maximus cursus lorem, nec laoreet velit eleifend vel. Ut aliquet mauris tortor, sed egestas libero interdum vitae. Fusce sed commodo purus, at tempus turpis."
          },
          {
            title  : "Y a t'il une règle de conduite pendant ce Hackathon ?",
            content: "Nunc maximus, magna at ultricies elementum, risus turpis vulputate quam."
          },
          {
            title  : "Que faire si ma question n'est pas mentionnée dans la FAQ ?",
            content: "Nunc maximus, magna at ultricies elementum, risus turpis vulputate quam."
          },]
      }

      const COLORS = {
        BLACK      : '#1b2145',
        DARK       : '#212121',
        GREY       : '#75798f',
        SILVER     : '#f8f8f8',
        WHITE      : 'ffffff'
      }

      const styles     = {
        bgColor        : COLORS.WHITE,
        titleTextColor : COLORS.BLACK,
        rowTitleColor  : COLORS.DARK,
        rowContentColor: COLORS.GREY,
        arrowColor     : COLORS.BLACK
      };

      const config     = {
        animate        : true,
        arrowIcon      : "+",
        tabFocus       : true
      };

    return ( 
        <>
         <div className="rf-container">
            <div className="rf-grid-row rf-grid-row--gutters rf-mt-5w">
                <div className="rf-col">
                    <h2>Foire aux questions</h2>
                </div>
            </div>
            <div className="rf-grid-row rf-grid-row--gutters rf-mb-5w">
                <div className="rf-col-12">
                <Faq 
                    id="FrequentlyAskedQuestions"
                    data={data}
                    styles={styles}
                    config={config}
                />
                </div>
            </div>
        </div>
        </>
     );
}
 
export default FrequentlyAskedQuestions;