import { Link } from "react-router-dom"
import DarkMode from "./DarkMode"

const Header = () => {
    return ( 
        <header className="rf-header" role="banner">
            <div className="rf-skiplinks">
                <div className="rf-container">
                    <ul className="rf-skiplinks__list">
                        <li>
                            <a className="rf-link" href="#contenu">Accéder au contenu</a>
                        </li>
                        <li>
                            <a className="rf-link" href="#header-navigation">Accéder au menu</a>
                        </li>
                        <li>
                            <a className="rf-link" href="#header-search">Accéder à la recherche</a>
                        </li>
                        <li>
                            <a className="rf-link" href="#footer">Accéder au footer</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="rf-container">
                <div className="rf-header__body">
                    <div className="rf-header__brand">
                        <a title="Ministère des Armées" className="rf-logo" href="https://www.defense.gouv.fr/" target="_blank">
                            <span className="rf-logo__title">
                            <span>Ministère<br/>des Armées</span></span>
                        </a>
                    </div>
                    <div className="rf-header__navbar">
                        <div className="rf-service">
                        <Link to="/" className="rf-service__title" href="#" title="Civils de la Défense">Civils de la Défense</Link>
                            <p className="rf-service__tagline">Hackathon GES / Eductive - 2021</p>
                        </div>
                    </div>
                    <div className="rf-header__tools">
                        <div className="rf-shortcuts">
                            <ul className="rf-shortcuts__list">
                                <li className="rf-shortcuts__item">
                                    <Link to="arborescence" className="rf-link rf-fi-eye-line rf-link--icon-left" target="_self">Arborescence</Link>
                                </li>
                                <li className="rf-shortcuts__item">
                                    <Link to="articles" className="rf-link rf-fi-edit-line rf-link--icon-left" target="_self" id="formation">Dispositifs de formations</Link>
                                </li>

                                <li className="rf-shortcuts__item">
                                    <Link to="rappel" className="rf-link rf-fi-information-line rf-link--icon-left" target="_self">Memo</Link>
                                </li>

                                <li className="rf-shortcuts__item">
                                    <Link to="contact"className="rf-link rf-fi-account-line" target="_self">Contact</Link>
                                </li>

                                <li className="rf-shortcuts__item">
                                    <DarkMode/>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>   
    );
}
 
export default Header;