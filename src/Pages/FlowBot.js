import React, { useState } from 'react';
import ReactFlow, { Controls, addEdge, Background, MiniMap } from 'react-flow-renderer';
import {Link} from 'react-router-dom';

const initialElements = [
  // 0
  {id: '1', type: 'input', data: { label: (<>Bonjour,  je suis <br/>"nom du chat bot". <br/><br/>Je suis à votre disposition pour vous orienter et vous aider dans votre parcours professionnel</>) }, position: { x: 250, y: -200 }},

  // 00
  { id: '2', data: { label: 'Que souhaitez-vous faire ?' }, position: { x: 250, y: 100 } },
  { id: '3', data: { label: "Me former" }, position: { x: -850, y: 300 } },
  { id: '4', data: { label: "Faire évoluer ma situation professionnelle actuelle" }, position: { x: 0, y: 300 } },
  { id: '5', data: { label: "Faire le point sur ma carrière" }, position: { x: 750, y: 300 } },
  { id: '6', data: { label: "Faire reconnaitre mes connaissances" }, position: { x: 1400, y: 300 } },  

  // I 
  { id: '7', data: { label: "Sélectionnez parmi ces choix dans quel but souhaitez-vous vous former ?" }, position: { x: -850, y: 400 } },
  // 1
  { id: '8', data: { label: "Pour acquérir des compétences en lien avec mon poste de travail actuel" }, position: { x: -1000, y: 600 } },
  { id: '9', data: { label: "Je vous propose de consulter le catalogue de l'offre ministérielle afin de trouver une formation qui correspond à vos besoins, et par la suite, formuler une demande lors du CREP" } , position: { x: -1000, y: 750 } },
  { id: '10', type: "output", data: { label: (<>Vous pouvez y accéder directement en cliquant sur ce lien <br/><a href="https://offre-formation.intradef.gouv.fr">https://offre-formation.intradef.gouv.fr</a></>) } , position: { x: -1000, y: 1000 } },
  // 2

  { id: '11', data: { label: "Pour me réorienter professionnellement" }, position: { x: -700, y: 600 } },
  { id: '12', type: "output", data: { label: "Si vous souhaitez une formation de courte ou moyenne durée, vous avez la possibilité de mobiliser vos droits CPF " }, position: { x: -800, y: 750 } },
  { id: '13', type: "output", data: { label: "Si vous souhaitez une formation de longue durée, vous avez la possibilité de faire une demande de congé de formation professionnelle" }, position: { x: -600, y: 750 } },


  // II
  { id: '14', data: { label: "Sélectionnez parmi ces choix votre souhait d'évolution" }, position: { x: 0, y: 400 } },
  // 1
  { id: '15', data: { label: "Changer de métier" }, position: { x: -200, y: 600 } },
  { id: '16', type: "output", data: { label: "Si vous souhaitez une formation de courte ou moyenne durée, vous avez la possibilité de mobiliser vos droits CPF " }, position: { x: -300, y: 750 } },
  { id: '17', type: "output", data: { label: "Si vous souhaitez une formation de longue durée, vous avez la possibilité de formuler une demande de congé de formation professionnelle, ou d'avoir recours à la période de professionnalisation" }, position: { x: -100, y: 750 } },
  // 2
  { id: '18', data: { label: "Changer de grade ou de corps" }, position: { x: 200, y: 600 } },
  { id: '19', type: "output", data: { label: "Concours Interne" }, position: { x: 100, y: 750 } },
  { id: '20', type: "output", data: { label: "Examen Professionnel" }, position: { x: 300, y: 750 } },


  // III
  { id: '21', data: { label: "Sélectionnez parmi ces choix de quelle manière souhaitez vous faire le point" }, position: { x: 750, y: 400 } },

  // 1
  { id: '22', data: { label: "En rencontrant un conseiller mobilité carrière" }, position: { x: 600, y: 600 } },
  { id: '23', type: "output", data: { label: (<>Vous pouvez trouver votre conseiller mobilité carrière en cliquant directement sur ce lien <br/><a href="https://www.defense-mobilite.fr/">https://www.defense-mobilite.fr/</a></>) }, position: { x: 600, y: 750 } },
  // 2
  { id: '24', data: { label: "En effectuant un bilan de compétences" }, position: { x: 900, y: 600 } },
  { id: '25', type: "output", data: { label: "Vous pouvez formuler une demande lors du CREP ou de l'entretien annuel de formation" }, position: { x: 800, y: 750 } },
  { id: '26', type: "output", data: { label: "Il est conseillé de prendre attache avec un CMC au préalable " }, position: { x: 1000, y: 750 } },

  // IV
  { id: '27', data: { label: "Sélectionnez parmi ces choix ce qui vous correspond le plus" }, position: { x: 1400, y: 400 } },
  { id: '28', data: { label: "Je veux obtenir mon diplôme sanctionnant des compétences dont je dispose déjà" }, position: { x: 1300, y: 600 } },
  { id: '29', type: "output", data: { label: "Vous pouvez effectuer une de demande de Validation des acquis de l'expérience auprès de votre responsable de formation" }, position: { x: 1300, y: 750 } },
  { id: '30', data: { label: "Bilan de compétences ou bilan de compétences et renvoi MTD" }, position: { x: 1500, y: 600 } },
  { id: '31', type: "output", data: { label: "Site" }, position: { x: 1500, y: 750 } },


  // Animation
  { id: 'e1-2', source: '1', target: '2' },
  { id: 'e2-3', source: '2', target: '3', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },
  { id: 'e2-3', source: '2', target: '4', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },
  { id: 'e2-3', source: '2', target: '5', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },
  { id: 'e2-3', source: '2', target: '6', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },

  { id: 'e2-3', source: '3', target: '7', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '7', target: '8', arrowHeadType: 'arrowclosed', type: 'smoothstep', label: 'Choix', style: { stroke: '#f6ab6c' }, labelStyle: { fill: '#f6ab6c', fontWeight: 700 }  },
  { id: 'e2-3', source: '8', target: '9', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '9', target: '10', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '7', target: '11', arrowHeadType: 'arrowclosed', type: 'smoothstep', label: 'Choix', style: { stroke: '#f6ab6c' }, labelStyle: { fill: '#f6ab6c', fontWeight: 700 }  },
  { id: 'e2-3', source: '11', target: '12', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '11', target: '13', arrowHeadType: 'arrowclosed', type: 'smoothstep' },

  { id: 'e2-3', source: '4', target: '14', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '14', target: '15', arrowHeadType: 'arrowclosed', type: 'smoothstep', label: 'Choix', style: { stroke: '#f6ab6c' }, labelStyle: { fill: '#f6ab6c', fontWeight: 700 }  },
  { id: 'e2-3', source: '15', target: '16', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '15', target: '17', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '14', target: '18', arrowHeadType: 'arrowclosed', type: 'smoothstep', label: 'Choix', style: { stroke: '#f6ab6c' }, labelStyle: { fill: '#f6ab6c', fontWeight: 700 }  },
  { id: 'e2-3', source: '18', target: '19', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '18', target: '20', arrowHeadType: 'arrowclosed', type: 'smoothstep' },

  { id: 'e2-3', source: '5', target: '21', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '21', target: '22', arrowHeadType: 'arrowclosed', type: 'smoothstep', label: 'Choix', style: { stroke: '#f6ab6c' }, labelStyle: { fill: '#f6ab6c', fontWeight: 700 }  },
  { id: 'e2-3', source: '22', target: '23', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '21', target: '24', arrowHeadType: 'arrowclosed', type: 'smoothstep', label: 'Choix', style: { stroke: '#f6ab6c' }, labelStyle: { fill: '#f6ab6c', fontWeight: 700 }  },
  { id: 'e2-3', source: '24', target: '25', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '24', target: '26', arrowHeadType: 'arrowclosed', type: 'smoothstep' },

  { id: 'e2-3', source: '6', target: '27', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '27', target: '28', arrowHeadType: 'arrowclosed', type: 'smoothstep', label: 'Choix', style: { stroke: '#f6ab6c' }, labelStyle: { fill: '#f6ab6c', fontWeight: 700 }  },
  { id: 'e2-3', source: '28', target: '29', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '27', target: '30', arrowHeadType: 'arrowclosed', type: 'smoothstep', label: 'Choix', style: { stroke: '#f6ab6c' }, labelStyle: { fill: '#f6ab6c', fontWeight: 700 }  },
  { id: 'e2-3', source: '30', target: '31', arrowHeadType: 'arrowclosed', type: 'smoothstep' },




  // { id: 'e3-4', source: '3', target: '4', animated: true, label: 'animated edge' },
];

const FlowBot = () => {
  const [elements, setElements] = useState(initialElements);
  const onLoad = (reactFlowInstance) => reactFlowInstance.fitView();
  
  const onConnect = (params) => setElements((els) => addEdge(params, els));
  return (
    <main style={{height:('100vh')}} id="flow">
      <Link to="/" className="rf-link rf-fi-arrow-left-line rf-link--icon-left" target="_self" id="BackHome">retour</Link>
      <ReactFlow
      elements={elements}
      onLoad={onLoad}
      snapToGrid
      minZoom={0}
      onConnect={onConnect}
    >
      <MiniMap
        nodeColor={(node) => {
          switch (node.type) {
            case 'input':
              return 'lightblue';
            case 'default':
              return 'lightgrey';
            case 'output':
              return 'rgb(0,0,255)';
            default:
              return '#eee';
          }
        }}
        nodeStrokeWidth={0}
      />

      <Background variant="lines" />
      <Controls />

    </ReactFlow>
    </main>
  );
};

export default FlowBot;