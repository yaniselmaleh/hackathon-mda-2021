import Header from "../Components/Header";
import Promote from "../Components/Promote";
import Cartes from "../Components/Rubriques"
import Footer from "../Components/Footer";

const Main = () => {
    return ( 
       <>
         <Header/>
         <Promote/>
         <Cartes/>
         <Footer/>
   </> );
}
 
export default Main;