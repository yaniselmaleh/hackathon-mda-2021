import React, { useState } from 'react';
import ReactFlow, { Controls, addEdge, Background, MiniMap } from 'react-flow-renderer';
import {Link} from 'react-router-dom';

const initialElements = [
  // 0
  {id: '1', type: 'input', data: { label: (<>Briques d’information souhaitées sur le site vitrine en front office <br/><br/>(architecture à votre main !)</>) }, position: { x: 250, y: -200 }},

  // 00
  { id: '2', data: { label: 'Accueil avec page actualité' }, position: { x: 250, y: 100 } },

  { id: '3', data: { label: "Trouver mon dispositif" }, position: { x: -400, y: 300 } },
  { id: '4', data: { label: "Les différents dispositifs de formation" }, position: { x: -100, y: 300 } },
  { id: '5', data: { label: "Le catalogue de formation" }, position: { x: 250, y: 300 } },
  { id: '6', data: { label: "Devenir formateur interne" }, position: { x: 600, y: 300 } },  
  { id: '7', data: { label: "Annuaire" }, position: { x: 850, y: 300 } },  
  { id: '8', data: { label: "Les ressources réglementaires" }, position: { x: 1100, y: 300 } },  

  { id: '9', type: "output", data: { label: "Outil chatbot" }, position: { x: -400, y: 400 } },
  { id: '10', type: "output", data: { label: "CPF / CFP / Préparation concours / Parcours professionalisants..." }, position: { x: -100, y: 400 } },
  { id: '11', type: "output", data: { label: "Lien externe catalogue" }, position: { x: 150, y: 400 } },
  { id: '12', type: "output", data: { label: "Lien externe plateforme e-learning" }, position: { x: 350, y: 400 } },
  { id: '13', type: "output", data: { label: "Devenir formateur interne occasionnel (lien vers un formulaire qui n’existe pas encore aujourd’hui)" }, position: { x: 600, y: 400 } },
  { id: '14', data: { label: "L’équipe (SRHC + CMG)" }, position: { x: 850, y: 400 } },
  { id: '15', type: "output", data: { label: "Annuaire interactif des 400 responsables de formation" }, position: { x: 850, y: 500 } },
  

  // Animation
  { id: 'e1-2', source: '1', target: '2' },
  { id: 'e2-3', source: '2', target: '3', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },
  { id: 'e2-3', source: '2', target: '4', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },
  { id: 'e2-3', source: '2', target: '5', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },
  { id: 'e2-3', source: '2', target: '6', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },
  { id: 'e2-3', source: '2', target: '7', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },
  { id: 'e2-3', source: '2', target: '8', arrowHeadType: 'arrowclosed', type: 'smoothstep', animated: true },

  { id: 'e2-3', source: '3', target: '9', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '4', target: '10', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '5', target: '11', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '5', target: '12', arrowHeadType: 'arrowclosed', type: 'smoothstep' },

  { id: 'e2-3', source: '6', target: '13', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '7', target: '14', arrowHeadType: 'arrowclosed', type: 'smoothstep' },
  { id: 'e2-3', source: '14', target: '15', arrowHeadType: 'arrowclosed', type: 'smoothstep' },

  { id: 'e2-3', source: '9', target: '10', arrowHeadType: 'arrowclosed', type: 'smoothstep', label: 'Choix', style: { stroke: '#f6ab6c' }, labelStyle: { fill: '#f6ab6c', fontWeight: 700 }  },
  

  // { id: 'e3-4', source: '3', target: '4', animated: true, label: 'animated edge' },
];

const FlowWeb = () => {
  const [elements, setElements] = useState(initialElements);
  const onLoad = (reactFlowInstance) => reactFlowInstance.fitView();
  
  const onConnect = (params) => setElements((els) => addEdge(params, els));
  return (
    <main style={{height:('100vh')}} id="flow">
      <Link to="/" className="rf-link rf-fi-arrow-left-line rf-link--icon-left" target="_self" id="BackHome">retour</Link>
      <ReactFlow
      elements={elements}
      onLoad={onLoad}
      snapToGrid
      minZoom={0}
      onConnect={onConnect}
    >
      <MiniMap
        nodeColor={(node) => {
          switch (node.type) {
            case 'input':
              return 'lightblue';
            case 'default':
              return 'lightgrey';
            case 'output':
              return 'rgb(0,0,255)';
            default:
              return '#eee';
          }
        }}
        nodeStrokeWidth={0}
      />

      <Background variant="lines" />
      <Controls />

    </ReactFlow>
    </main>
  );
};

export default FlowWeb;