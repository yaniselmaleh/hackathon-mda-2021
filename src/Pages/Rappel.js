import Header from "../Components/Header"
import Footer from "../Components/Footer"

const Rappel = () => {
    return ( 
        <>
        <Header/>
        <div className="rf-container rf-my-5w">
            <h1>Rappel sur les <u>challenges</u></h1>
            <p>Deux challenges sont imposés dans le cadre du Hackathon, avec des <b>contraintes</b> d’accessibilité, de design,
                de sécurité et de communication etc.</p>
            <ul>
                <li>→ Création d’un site vitrine</li>
                <li>→ Création d’un chatbot</li>
            </ul>
        </div>   

        <div className="rf-container rf-grid-row rf-grid-row-gutters">
            <div className="rf-col-12 rf-col-lg-6 rf-mt-5w">
                <h2>Memo</h2>
                <ul>
                    <li>→ Site entièrement administrable</li>
                    <li>→ Chatbot entièrement administrable</li>
                    <li>→ Un respect de la Charte graphique</li>
                    <li>→ Les logos notament du chatbot devront être non militaire et non genré</li>
                    <li>→ Intuitif et simple d’utilisation</li>
                    <li>→ Nous sommes disponibles à tout moment sur Teams</li>
                    <li>→ <b>Respect de l'accord de confidentialité</b> du Hackaton</li>
                </ul>
            </div>

            <div className="rf-col-12 rf-col-lg-6 rf-mt-5w rf-callout rf-scheme-soft-blue-soft rf-mb-8v">
                <h2>Contraintes</h2>
                <ul>
                    <li>0. Confidentialité</li>
                    <li>1. Contrainte Design</li>
                    <li>2. Contrainte Logos</li>
                    <li>3. Contraintes sécuritaires</li>
                    <li>4. Contraintes techniques</li>
                    <li>5. Contraintes d’accessibilité</li>
                </ul>
            </div>
        </div>
        <div className="rf-container rf-grid-row rf-grid-row-gutters rf-mb-8v">
            <div className="rf-col-12 rf-mt-5w">
                <h3>Fonctionnalités</h3>
                <p className="rf-text">Les principales fonctionnalités attendues sur l’outil sont :	
                    <ul>
                        <li><b>→</b> un site vitrine accessible depuis internet ou intranet et entièrement responsive</li>
                        <li><b>→</b> un chatbot qui permettra aux agents de poser leurs questions et de trouver le dispositif adapté à leurs besoins.</li>
                    </ul> 
                </p>

                <br/>

                <p className="rf-text">	
                    L’outil devra être entièrement administrable (contenus et visuels) par nos soins et pouvoir s’alimenter des réponses apportées par les agents (machine learning) afin d’offrir une expérience plus immersive, plus orientée avec une offre adaptée aux besoins et profil de l’agent.
                    <br/><br/>
                    Les fonctionnalités devront permettre au terme du processus à l’agent d’obtenir une restitution compréhensible et utilisable lui indiquant la procédure à mettre en œuvre avec les étapes, critères et acteurs, tout ceci dans le respect des contraintes sécuritaires et techniques du ministère.              
                </p> 
            </div> 

            <div className="rf-col-12 rf-mt-5w">
                <h3>Site Web</h3>
                <p className="rf-text">Le site vitrine, accessible depuis l’intranet ou internet, devra permettre aux utilisateurs de découvrir l’offre de formation et s’orienter dans les dispositifs existants. Un chatbot offrant une expérience dynamique et ludique à l’usager, constituera l’élément central de ce site.</p>
                <br/>
                <p className="rf-text">Le site vitrine doit :</p>
                <ul>
                    <li><b>→</b> être administrable en contenu et en images </li>
                    <li><b>→</b> répondre le plus possible aux critères d’accessibilité numérique	</li>
                </ul>           
            </div>  

            <div className="rf-col-12 rf-mt-5w">
                <h3>ChatBot</h3>
                <p className="rf-text">Naming :</p>
                <ul>
                    <li><b>→</b> Il s’agira de trouver un visuel pour le Chatbot, un logo ainsi qu’un nom.</li>
                    <li><b>→</b> A bannir : les représentations genrées ou issues de l’imaginaire militaire</li>
                </ul> 

                <br/>

                <p className="rf-text">Le chatbot proposera une série de questions à l’utilisateur qui par ses choix successifs orientera le programme qui lui indiquera in fine :</p>
                
                <ul>
                    <li><b>→</b> le(s) dispositif(s) de formation auquel il peut prétendre</li>
                    <li><b>→</b> fournira les informations et étapes pour en bénéficier</li>
                    <li><b>→</b> les points de contact, le cas échéant</li>
                    <li><b>→</b> mettra à disposition la documentation y afférant.</li>
                </ul>  

                <br/>
                <p className="rf-text">Les échanges avec le Chatbot se feront sur la base de questions-réponses pré-enregistrées. Les choix successifs se feront sous la forme de « quick replies », des boutons intégrés dans la conversation avec les différents choix de réponses. Un arbre décisionnel des différents échanges possibles avec le chatbot est fourni en annexe ainsi qu’une base de connaissances.</p>        
            </div>

            <div className="rf-col-12 rf-mt-5w">
                <h3>Contraintes</h3>
                <ul>
                    <li><b>→</b> Contraintes sécuritaires</li>
                </ul>           
                <p className="rf-text">L’outil devra respecter les normes de sécurité, détaillées dans le référentiel suivant : <a href="/Hackathon_CCT_MDA_Confidentiel.pdf" download="" title="Hackathon_CCT_MDA" rel="noreferrer" target="_blank">Hackathon CCT MDA</a></p> 
            
                <br/>
                <ul>
                    <li><b>→</b> Contraintes d’accessibilité</li>
                </ul>           
                <p className="rf-text">Pour les critères d’accessibilité veuillez-vous référer au lien suivant : <a href="https://github.com/DISIC/RGAA" title="RGAA - [Ouvrir dans une nouvelle fenêtre]" target="_blank">https://github.com/DISIC/RGAA</a></p> 
            
                <br/>
                <ul>
                    <li><b>→</b> Contraintes design</li>
                </ul>           
                <p className="rf-text">Le site devra respecter la charte graphique internet de l’Etat suivante : <a href="https://www.gouvernement.fr/charte/charte-graphique-les-fondamentaux/introduction" title="RGAA - [Ouvrir dans une nouvelle fenêtre]" target="_blank">Charte graphique les fondamentaux</a></p> 
            </div>  

            <div className="rf-col-12 rf-mt-5w">
                <h3>Évolutions</h3>
                <p className="rf-text">L’outil doit être évolutif et ouvert afin de pouvoir développer par la suite les fonctionnalités décrites ci-dessous :</p>       
                <ul>
                    <li><b>→</b> L’outil sera accessible sans collecte de données personnelles, ni création de compte.</li>
                    <li><b>→</b> Interfaçage futur avec le catalogue de formation (ALF) du ministère.</li>
                </ul>    
                <br/>
                <p className="rf-text">A terme il est envisagé que le Chatbot puisse proposer des formations disponibles au catalogue et ouvertes aux inscriptions en fonction des thèmes et compétences indiqués par l’utilisateur grâce à une recherche par mots clés.</p>       
            </div>  
        </div>
        <Footer/>
        </>
     );
}
 
export default Rappel;