import Header from "../Components/Header"

const Contact = () => {
    return ( 
        <>
            <Header/>
            <div className="rf-container rf-my-5w">
                <h1>Point de <u>Contact</u></h1>
                <p>Les demandes ne concernant pas les champs d'action du Hackathon ou du Ministère des Armées n'obtiendront pas de réponse. Nous vous rappelons que les propos à caractère injurieux, racistes ou diffamatoires, constituent des délits sanctionnés par la loi.</p>
            </div>   
                
            <div className="rf-container rf-grid-row rf-grid-row-gutters">
                <div className="rf-col-12 rf-col-lg-12 rf-mt-5w">
                    <h2>Équipe projet</h2>
                    <ul>
                        <li>→ Amandine PECHALAT</li>
                        <li>→ Victoria ORSINI</li>
                        <li>→ Patrice D’AVILA</li>
                        <li>→ Nihad MOSTEGHANEMI</li>
                        <li>→ Yanis ELMALEH (Coach) - yanis.elmaleh@intradef.gouv.fr</li>
                    </ul>
                </div>
            </div>
        </>
     );
}
 
export default Contact;