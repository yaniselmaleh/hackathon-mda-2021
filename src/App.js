import React, { useEffect ,useState, lazy, Suspense } from 'react'

import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Spinner from "./Components/Loader/Spinner"
import NotFound from './Components/NotFound';
import Lexiques from './Components/Lexiques';

const TreeChoice = lazy(() => import("./Components/TreeChoice"));
const FlowWeb = lazy(() => import("./Pages/FlowWeb"));
const FlowBot = lazy(() => import("./Pages/FlowBot"));
const Rappel = lazy(() => import("./Pages/Rappel"));
const Main = lazy(() => import("./Pages/Main"));
const Contact = lazy(() => import("./Pages/Contact"));
const App = () => {
  const [lexiques, setLexiques] = useState([]);

  useEffect(() => {
    fetch("https://hackathon-mda-api-2021.herokuapp.com/api/data.json").then(res => res.json()).then(res => {
      setLexiques(res)
    });
  }, []);

  return (
    <Suspense fallback={<Spinner />}>
    <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Main} />
      <Route exact path="/arborescence" component={TreeChoice} />
      <Route exact path="/arborescence-web" component={FlowWeb} />
      <Route exact path="/arborescence-bot" component={FlowBot} />
      <Route exact path="/rappel" component={Rappel} />
      <Route exact path="/contact" component={Contact} />
      <Route path="/articles" component={() => <Lexiques lexiques={lexiques}/>}/>
      <Route path="*" component={NotFound} />
    </Switch>
  </BrowserRouter>
  </Suspense>
  )
};

export default App